### Lernzettel für Algorithmen und Datenstrukturen

#### Algorithmus-Begriff und Turing-Maschine

**Definition von Algorithmen:**
- **Eindeutige Handlungsvorschrift:** Ein Algorithmus ist eine festgelegte Schritt-für-Schritt-Anleitung zur Lösung eines Problems.
- **Endlichkeit:** Muss nach einer endlichen Anzahl von Schritten zu einem Ergebnis führen.
- **Determinismus:** Bei gleichen Eingaben liefert der Algorithmus immer dasselbe Ergebnis.

**Eigenschaften von Algorithmen:**
- **Eindeutigkeit:** Jeder Schritt des Algorithmus muss klar definiert sein.
- **Ausführbarkeit:** Jeder Schritt muss tatsächlich durchführbar sein.
- **Endlichkeit:** Der Algorithmus muss garantiert nach endlich vielen Schritten ein Ergebnis liefern.
- **Determinismus:** Bei gleichen Eingaben wird immer dasselbe Ergebnis erzielt.

**Darstellungsformen von Algorithmen:**
- **Pseudocode:** Eine textuelle Darstellung, die unabhängig von einer bestimmten Programmiersprache ist.
- **Programmcode:** Implementierung in einer Programmiersprache wie C, Java oder Python.
- **Flussdiagramme:** Grafische Darstellung von Prozessen und Entscheidungen in einem Algorithmus.
- **UML-Aktivitätsdiagramme:** Diagramme, die den Ablauf von Aktivitäten und Entscheidungen darstellen.

**Beispiele für Algorithmen:**
- **Bubble Sort:** Sortiert ein Array durch wiederholtes Vertauschen benachbarter Elemente.
- **Binary Search:** Durchsucht ein sortiertes Array, indem es das Suchgebiet in jeder Iteration halbiert.
- **Fibonacci-Zahlenfolge:** Berechnet die Fibonacci-Zahlen rekursiv oder iterativ.

**Turing-Maschine:**
- **Modell:** Ein theoretisches Modell der Berechenbarkeit, bestehend aus einem unendlichen Band und einem Kopf, der das Band liest und schreibt.
- **Komponenten:** Zustandsmenge \(Z\), Eingabealphabet \(\Sigma\), Bandalphabet \(\Gamma\), Übergangsfunktion \(\delta\), Anfangszustand \(z_0\), Endzustände \(E\).
- **Beispiel:** Eine Turing-Maschine, die zwei binäre Zahlen addiert, besteht aus Zuständen und Übergängen, die die Bits liest, addiert und das Ergebnis zurückschreibt.

**Beispiel einer Turing-Maschine:**
- **Addition von Binärzahlen:**
  - Zustände: \(q_0, q_1, q_2, q_f\)
  - Bandalphabet: \(0, 1, \text{Blank}\)
  - Übergangsfunktion:
    - \( (q_0, 0) \to (q_0, 0, R) \)
    - \( (q_0, 1) \to (q_1, 0, R) \)
    - \( (q_1, 0) \to (q_f, 1, S) \)
    - \( (q_1, 1) \to (q_1, 0, R) \)

#### Komplexitätsklassen/O-Kalkül

**Komplexität:**
- **Laufzeitkomplexität:** Zeitaufwand eines Algorithmus in Abhängigkeit von der Problemgröße \(n\).
- **Speicherkomplexität:** Speicherbedarf eines Algorithmus in Abhängigkeit von der Problemgröße \(n\).

**O-Kalkül:**
- **O-Notation (Obere Schranke):** Beschreibt das asymptotische Wachstum eines Algorithmus im schlimmsten Fall.
  \[
  f(n) \in O(g(n)) \iff \exists c > 0, \exists n_0 \in \mathbb{N}, \forall n \geq n_0 : f(n) \leq c \cdot g(n)
  \]
- **Ω-Notation (Untere Schranke):** Beschreibt die minimale Wachstumsrate eines Algorithmus.
  \[
  f(n) \in \Omega(g(n)) \iff \exists c > 0, \exists n_0 \in \mathbb{N}, \forall n \geq n_0 : f(n) \geq c \cdot g(n)
  \]
- **Θ-Notation (Genaue Schranke):** Beschreibt das genaue asymptotische Wachstum eines Algorithmus.
  \[
  f(n) \in \Theta(g(n)) \iff \exists c_1, c_2 > 0, \exists n_0 \in \mathbb{N}, \forall n \geq n_0 : c_1 \cdot g(n) \leq f(n) \leq c_2 \cdot g(n)
  \]

**Laufzeitmessung:**
- **Empirische Methoden:** Messen der tatsächlichen Ausführungszeit auf einem Rechner.
- **Theoretische Methoden:** Analysieren der Algorithmen anhand der Anzahl der Schritte in Abhängigkeit von der Problemgröße \(n\).

**Beispiel: Laufzeit von Bubble Sort**
- Im schlimmsten Fall (umgekehrt sortiertes Array): \(O(n^2)\)
- Im besten Fall (bereits sortiertes Array): \(O(n)\)

**Rechenbeispiel:**
- **Laufzeit von Bubble Sort für ein Array der Größe \(n=5\):**
  - Array: [5, 4, 3, 2, 1]
  - Schritte:
    - 1. Durchlauf: [4, 3, 2, 1, 5]
    - 2. Durchlauf: [3, 2, 1, 4, 5]
    - 3. Durchlauf: [2, 1, 3, 4, 5]
    - 4. Durchlauf: [1, 2, 3, 4, 5]
  - Insgesamt 10 Vergleiche und 10 Vertauschungen, also \(O(n^2)\).

#### Lineare Datenstrukturen

**Definition:**
- **Arrays:** Eine feste Größe mit direktem Zugriff auf jedes Element.
- **Listen:** Dynamische Größe mit sequentiellem Zugriff auf die Elemente.
- **Stacks:** LIFO (Last In, First Out) Prinzip, z.B. für die Rückverfolgung von Funktionsaufrufen.
- **Queues:** FIFO (First In, First Out) Prinzip, z.B. für Warteschlangen.

**Operationen:**
- **Array:**
  - Zugriff: \(O(1)\)
  - Einfügen/Löschen: \(O(n)\)
- **Liste:**
  - Zugriff: \(O(n)\)
  - Einfügen/Löschen: \(O(1)\) bei bekanntem Knoten
- **Stack:**
  - Push/Pop: \(O(1)\)
- **Queue:**
  - Enqueue/Dequeue: \(O(1)\)

**Beispiele:**
- **Array:** Ein Feld von Noten für Schüler.
  - **Beispiel:** `int noten[5] = {1, 2, 3, 4, 5};`
- **Liste:** Eine Einkaufsliste, bei der man Elemente hinzufügen oder entfernen kann.
  - **Beispiel:** `linked_list.add("Apfel");`
- **Stack:** Rückverfolgung von Funktionsaufrufen in einem Programm.
  - **Beispiel:** `stack.push(10);`
- **Queue:** Warteschlange im Druckerspooler.
  - **Beispiel:** `queue.enqueue("Druckauftrag");`

#### Suchalgorithmen

**Lineare Suche:**
- Durchsucht ein Array oder eine Liste sequenziell.
- Laufzeit: \(O(n)\)
- **Beispiel:** Suche nach einem bestimmten Namen in einer unsortierten Liste.
  - **Pseudocode:**
    ```python
    def linear_search(arr, target):
        for i in range(len(arr)):
            if arr[i] == target:
                return i
        return -1
    ```

**Binäre Suche:**
- Funktioniert auf sortierten Arrays/Listen.
- Teilt das Suchgebiet in jeder Iteration in zwei Hälften.
- Laufzeit: \(O(\log n)\)
- **Beispiel:** Suche nach einem bestimmten Buch in einem alphabetisch sortierten Bücherregal.
  - **Pseudocode:**
    ```python
    def binary_search(arr, target):
        left, right = 0, len(arr) - 1
        while left <= right:
            mid = (left + right) // 2
            if arr[mid] == target:
                return mid
            elif arr[mid] < target:
                left = mid + 1
            else:
                right = mid - 1
        return -1
    ```

#### Hashing

**Definition:**
- Verfahren zur schnellen Datenlokalisierung durch Transformation eines Schlüssels in einen Index des Arrays.

**Kollisionsbehandlung:**
- **Separate Chaining:** Jeder Array-Eintrag ist ein Zeiger auf eine Liste von Elementen.
- **Open Addressing:** Finden eines freien Platzes nach einer Kollision durch verschiedene Strategien (Linear Probing, Quadratic Probing).

**Beispiel:**
- **Hash-Tabelle:** Speicherung von Telefonnummern nach dem Namen. Bei einer Kollision (zwei Personen mit demselben Hashwert) wird eine Verkettungsliste verwendet.
  - **Pseudocode für Hashing mit Chaining:**
    ```python
    class

 HashTable:
        def __init__(self, size):
            self.size = size
            self.table = [[] for _ in range(size)]

        def hash_function(self, key):
            return hash(key) % self.size

        def insert(self, key, value):
            index = self.hash_function(key)
            self.table[index].append((key, value))

        def search(self, key):
            index = self.hash_function(key)
            for (k, v) in self.table[index]:
                if k == key:
                    return v
            return None
    ```

#### Sortieralgorithmen

**Bubble Sort:**
- Vergleich benachbarter Elemente und Vertauschung, wenn in falscher Reihenfolge.
- Laufzeit: \(O(n^2)\)
- **Beispiel:** Sortieren einer Liste von Zahlen in aufsteigender Reihenfolge.
  - **Pseudocode:**
    ```python
    def bubble_sort(arr):
        n = len(arr)
        for i in range(n):
            for j in range(0, n-i-1):
                if arr[j] > arr[j+1]:
                    arr[j], arr[j+1] = arr[j+1], arr[j]
    ```

**Merge Sort:**
- Divide-and-Conquer: Teilt das Array in zwei Hälften, sortiert rekursiv und verschmilzt dann.
- Laufzeit: \(O(n \log n)\)
- **Beispiel:** Sortieren einer Liste von Studenten nach ihren Noten.
  - **Pseudocode:**
    ```python
    def merge_sort(arr):
        if len(arr) > 1:
            mid = len(arr) // 2
            left_half = arr[:mid]
            right_half = arr[mid:]

            merge_sort(left_half)
            merge_sort(right_half)

            i = j = k = 0
            while i < len(left_half) and j < len(right_half):
                if left_half[i] < right_half[j]:
                    arr[k] = left_half[i]
                    i += 1
                else:
                    arr[k] = right_half[j]
                    j += 1
                k += 1

            while i < len(left_half):
                arr[k] = left_half[i]
                i += 1
                k += 1

            while j < len(right_half):
                arr[k] = right_half[j]
                j += 1
                k += 1
    ```

**Quick Sort:**
- Divide-and-Conquer: Wählt ein Pivot-Element, teilt das Array in Elemente kleiner und größer als das Pivot.
- Laufzeit: Durchschnittlich \(O(n \log n)\), im schlechtesten Fall \(O(n^2)\)
- **Beispiel:** Sortieren eines Kartendecks.
  - **Pseudocode:**
    ```python
    def quick_sort(arr):
        if len(arr) <= 1:
            return arr
        pivot = arr[len(arr) // 2]
        left = [x for x in arr if x < pivot]
        middle = [x for x in arr if x == pivot]
        right = [x for x in arr if x > pivot]
        return quick_sort(left) + middle + quick_sort(right)
    ```

#### Bäume

**Definition:**
- Hierarchische Datenstruktur aus Knoten, wobei jeder Knoten ein Elternknoten und null oder mehr Kindknoten hat.

**Typen:**
- **Binärbaum:** Jeder Knoten hat höchstens zwei Kinder.
- **AVL-Baum:** Selbstbalancierender Binärbaum.
- **B-Baum:** Selbstbalancierender Suchbaum, der mehr als zwei Kinder pro Knoten erlaubt.

**Operationen:**
- **Einfügen:** \(O(\log n)\) für balancierte Bäume.
- **Löschen:** \(O(\log n)\) für balancierte Bäume.
- **Durchsuchen:** \(O(\log n)\) für balancierte Bäume.

**Beispiel:**
- **Binärbaum:** Darstellung eines Stammbaums einer Familie.
  - **Pseudocode für Einfügen in einen Binärbaum:**
    ```python
    class TreeNode:
        def __init__(self, key):
            self.left = None
            self.right = None
            self.val = key

    def insert(root, key):
        if root is None:
            return TreeNode(key)
        else:
            if root.val < key:
                root.right = insert(root.right, key)
            else:
                root.left = insert(root.left, key)
        return root
    ```

#### Graphen

**Definition:**
- Eine Menge von Knoten (Ecken) und eine Menge von Kanten (Verbindungen zwischen Knoten).

**Darstellungsformen:**
- **Adjazenzmatrix:** 2D-Array, wobei der Eintrag (i, j) eine Kante zwischen Knoten i und Knoten j darstellt.
- **Adjazenzliste:** Liste von Listen, wobei jeder Knoten eine Liste seiner Nachbarn hat.

**Graphenalgorithmen:**
- **Tiefensuche (DFS):** Rekursiver Besuch aller Knoten eines Graphen.
  - **Beispiel:** Finden aller Knoten, die von einem bestimmten Knoten aus erreichbar sind.
  - **Pseudocode:**
    ```python
    def dfs(graph, start, visited=None):
        if visited is None:
            visited = set()
        visited.add(start)
        for next in graph[start] - visited:
            dfs(graph, next, visited)
        return visited
    ```

- **Breitensuche (BFS):** Besuch aller Knoten eines Graphen in Breitensuche-Reihenfolge.
  - **Beispiel:** Finden des kürzesten Pfads in einem ungewichteten Graphen.
  - **Pseudocode:**
    ```python
    from collections import deque

    def bfs(graph, start):
        visited = set()
        queue = deque([start])
        visited.add(start)
        while queue:
            vertex = queue.popleft()
            for neighbour in graph[vertex]:
                if neighbour not in visited:
                    visited.add(neighbour)
                    queue.append(neighbour)
        return visited
    ```

- **Dijkstra-Algorithmus:** Findet den kürzesten Pfad in einem gewichteten Graphen.
  - **Beispiel:** Finden der kürzesten Route in einem Straßennetz.
  - **Pseudocode:**
    ```python
    import heapq

    def dijkstra(graph, start):
        queue = []
        heapq.heappush(queue, (0, start))
        distances = {vertex: float('infinity') for vertex in graph}
        distances[start] = 0
        while queue:
            current_distance, current_vertex = heapq.heappop(queue)
            if current_distance > distances[current_vertex]:
                continue
            for neighbor, weight in graph[current_vertex].items():
                distance = current_distance + weight
                if distance < distances[neighbor]:
                    distances[neighbor] = distance
                    heapq.heappush(queue, (distance, neighbor))
        return distances
    ```

#### Brute-Force-Algorithmen

**Definition:**
- Einfachste und naivste Methode, ein Problem zu lösen, indem alle möglichen Lösungen durchprobiert werden.

**Eigenschaften:**
- Sehr ineffizient für große Probleme.
- Wird oft als Basis für die Entwicklung effizienterer Algorithmen verwendet.

**Beispiel:**
- **Travelling Salesman Problem (TSP):** Finden der kürzesten Route, die eine Liste von Städten besucht und am Ausgangspunkt endet, indem alle möglichen Permutationen der Städte geprüft werden.
  - **Pseudocode:**
    ```python
    from itertools import permutations

    def tsp(graph, start):
        vertices = list(graph.keys())
        vertices.remove(start)
        min_path = float('infinity')
        for perm in permutations(vertices):
            current_path = 0
            k = start
            for j in perm:
                current_path += graph[k][j]
                k = j
            current_path += graph[k][start]
            min_path = min(min_path, current_path)
        return min_path
    ```

- **Knapsack Problem:** Finden der wertvollsten Kombination von Gegenständen, die in einen Rucksack passen, indem alle möglichen Kombinationen geprüft werden.
  - **Pseudocode:**
    ```python
    def knapsack(values, weights, capacity):
        n = len(values)
        max_value = 0
        for i in range(2**n):
            total_weight = total_value = 0
            for j in range(n):
                if i & (1 << j):
                    total_weight += weights[j]
                    total_value += values[j]
            if total_weight <= capacity:
                max_value = max(max_value, total_value)
        return max_value
    ```

#### Zusammenfassung

- **Algorithmen und Datenstrukturen** bilden die Grundlage der Informatik.
- **Komplexitätsanalyse** ist entscheidend für die Bewertung der Effizienz von Algorithmen.
- **Lineare Datenstrukturen** und **Bäume** sind grundlegende Bausteine.
- **Such- und Sortieralgorithmen** sind essentielle Operationen in der Datenverarbeitung.
- **Graphen** modellieren Netzwerke und Beziehungen, mit spezifischen Algorithmen zur Analyse.
- **Brute-Force-Methoden** bieten einfache, aber oft ineffiziente Lösungen.

Diese detaillierte Zusammenfassung mit Beispielen, Pseudocode und Notizen sollte dir helfen, die wichtigsten Konzepte und Algorithmen besser zu verstehen und dich effektiv auf die Klausur vorzubereiten

. Falls du zusätzliche Erklärungen oder weitere Beispiele benötigst, lass es mich wissen!