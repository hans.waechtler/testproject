### Zusammenfassung des Skripts "Mikroprozessortechnik – Einführung"

#### 1. Organisation und Einführung
Das Modul behandelt die Grundlagen und fortgeschrittene Themen der Mikroprozessortechnik, insbesondere für die Fahrzeugtechnik. Der Inhalt ist in mehrere Bereiche gegliedert:
- Orga & Einführung
- Modellprozessor
- Architektur Atmel AVR RISC
- Maschinenoperationen
- Timer Counter AD Wandler
- Interruptverarbeitung
- Parallele und serielle IO
- Systembus
- Speicher
- RISC/CISC und Superskalarität

#### 2. Software im Automobil
- Mikrocontroller und ihre Rolle in der Elektronik- und Elektrik-Architektur (E/E-Architektur) eines Fahrzeugs.
- Der Prozess von der Softwareentwicklung (z.B. in C, C++, Java, MATLAB) bis zum Flashen der ausführbaren Dateien auf Mikrocontroller.

#### 3. Signal- und Datenverarbeitung
- Grundlegende Signalverarbeitung: Extraktion, Verstärkung oder Veränderung von Informationen.
- Beispielhafte Systeme wie ABS-/ESP-Systeme in Fahrzeugen.
- Unterschied zwischen analoger und digitaler Signalverarbeitung:
  - **Analog:** Mechanische, pneumatische und elektronische Regler.
  - **Digital (festverdrahtet):** Diskrete Logik, FPGA, ASIC.
  - **Digital (programmierbar):** Mikroprozessoren, Mikrocontroller, DSPs, FPGA mit Softcore.

#### 4. Modell eines einfachen 8-Bit-Prozessors
- **Hauptkomponenten:**
  - **Register (R0, R1):** Speichern Operanden und Rechenergebnisse.
  - **ALU (Arithmetic Logic Unit):** Führt arithmetische und logische Operationen durch.
  - **Steuerwerk:** Steuert alle Abläufe im Prozessor.
  - **Speicher:** Enthält Programmcodes und Daten.
  - **Busse:** Adress-, Daten- und Steuerbus zur Kommunikation zwischen den Komponenten.

- **Steuerung/Decodierer:**
  - **Program Counter (PC):** Zeigt auf den nächsten auszuführenden Befehl.
  - **Address Register (AR):** Dient zur Adressierung von Operanden im Speicher.
  - **Instruction Register (IR):** Speichert den aktuellen Befehl.

#### 5. Maschinenbefehle und Programmbeispiele
- **Befehlssatz:**
  - **Clr Rr:** Register Rr löschen.
  - **Ldm Rr, m:** Lade Speicherinhalt m in Register Rr.
  - **Stm m, Rr:** Speichere Registerinhalt Rr in Speicheradresse m.
  - **Add Rr, Rs:** Addiere Register Rs zu Register Rr.
  - **Sub Rr, Rs:** Subtrahiere Register Rs von Register Rr.
  - **Halt:** Prozessor anhalten.

- **Beispielprogramm:** Addition von vier 8-Bit-Zahlen und Speicherung des Ergebnisses:
  ```asm
  clr R0
  ldm R1, Adresse(A)
  add R0, R1
  ldm R1, Adresse(B)
  add R0, R1
  ldm R1, Adresse(C)
  add R0, R1
  ldm R1, Adresse(D)
  add R0, R1
  stm Adresse(E), R0
  halt
  ```

#### 6. Weiterführende Themen
- **Zusammenschaltung von Speicher und ALU:** Erweiterungsmöglichkeiten und Ablaufsteuerung.
- **Signalverarbeitung in Fahrzeugen:** Implementierung von ABS/ESP-Systemen und anderen eingebetteten Systemen.

### Zusammenfassung des Skripts "Modellprozessor"

#### 1. Einführung in den Modellprozessor
Das Skript beginnt mit der Vorstellung eines einfachen Modellprozessors, um die grundlegenden Prinzipien der Mikroprozessortechnik zu veranschaulichen. Dieser Modellprozessor ist ein 8-Bit-Prozessor und besteht aus den folgenden Hauptkomponenten:

- **Register:** R0 und R1 zur Speicherung von Operanden und Ergebnissen.
- **ALU (Arithmetic Logic Unit):** Führt arithmetische und logische Operationen durch.
- **Steuerwerk:** Steuert die Ausführung von Befehlen und die Abläufe im Prozessor.
- **Speicher:** Enthält Programmcodes und Daten.
- **Busse:** Adress-, Daten- und Steuerbus zur Kommunikation zwischen den Komponenten.

#### 2. Architektur des Modellprozessors
- **PC (Program Counter):** Zeigt auf den nächsten auszuführenden Befehl im Speicher.
- **AR (Address Register):** Dient zur Adressierung von Operanden im Speicher.
- **IR (Instruction Register):** Speichert den aktuellen Befehl.
- **Status Register:** Enthält Statusinformationen wie Carry, Zero und Negative Flags.

#### 3. Befehlssatz des Modellprozessors
- **Clr Rr:** Löscht den Inhalt des Registers Rr.
- **Ldm Rr, m:** Lädt den Wert aus Speicheradresse m in das Register Rr.
- **Stm m, Rr:** Speichert den Inhalt des Registers Rr in die Speicheradresse m.
- **Add Rr, Rs:** Addiert den Inhalt des Registers Rs zum Register Rr.
- **Sub Rr, Rs:** Subtrahiert den Inhalt des Registers Rs vom Register Rr.
- **Halt:** Stoppt die Ausführung des Prozessors.

#### 4. Programmablauf
Der Programmablauf des Modellprozessors umfasst die folgenden Phasen:

- **Fetch:** Laden des nächsten Befehls aus dem Speicher.
- **Decode:** Decodieren des geladenen Befehls.
- **Execute:** Ausführen des Befehls und Aktualisieren der Register und des Status Registers.

#### 5. Beispielprogramm
Das Skript enthält ein Beispielprogramm zur Addition von vier 8-Bit-Zahlen, das die grundlegenden Befehle und den Ablauf im Modellprozessor veranschaulicht:

```asm
clr R0
ldm R1, Adresse(A)
add R0, R1
ldm R1, Adresse(B)
add R0, R1
ldm R1, Adresse(C)
add R0, R1
ldm R1, Adresse(D)
add R0, R1
stm Adresse(E), R0
halt
```

#### 6. Ablaufsteuerung und Implementierung
Die Ablaufsteuerung des Modellprozessors wird durch das Steuerwerk realisiert, das die einzelnen Befehle nacheinander ausführt und dabei die entsprechenden Register und Speicheradressen anspricht. Die Implementierung erfolgt durch fest verdrahtete Logik, die den Fluss von Daten und Befehlen im Prozessor steuert.

#### 7. Erweiterungsmöglichkeiten
Das Skript beschreibt auch Erweiterungsmöglichkeiten des Modellprozessors, wie die Hinzufügung weiterer Register, komplexerer Rechenoperationen und erweiterter Speicherhierarchien.

### Zusammenfassung der wichtigsten Konzepte:
- **Grundkomponenten:** Register, ALU, Steuerwerk, Speicher, Busse.
- **Befehlssatz:** Basisbefehle für Datenmanipulation und Steuerung.
- **Programmablauf:** Fetch, Decode, Execute.
- **Erweiterungen:** Zusätzliche Register und komplexere Operationen.

### Zusammenfassung des Skripts "Architektur Atmel AVR-RISC"

#### 1. Einführung in die Architektur
- **RISC (Reduced Instruction Set Computer):** Der Atmel AVR-RISC Mikrocontroller basiert auf einer modernen 8-Bit-Architektur mit Harvard-Architektur, das heißt, er hat separate Speicher für Programme und Daten.
- **Harvard-Architektur:** Getrennte Busse für Daten- und Programmspeicher ermöglichen parallelen Zugriff auf Befehle und Daten.

#### 2. Hauptkomponenten des ATmega32
- **32 Register:** Schneller Zugriff und Operationen innerhalb der CPU.
- **Programmspeicher (Flash):** Bis zu 256 kByte (z.B. ATmega32 hat 32 kByte).
- **Datenspeicher (SRAM):** Bis zu 8 kByte (z.B. ATmega32 hat 2 kByte).
- **Peripheriekomponenten:** Parallele und serielle Schnittstellen (I2C, SPI), AD-Wandler, Timer, CAN, USB.

#### 3. Befehlsarchitektur
- **Befehlssatz des ATmega32:** Besteht aus Datentransfer-, Arithmetik-, Logik-, Schiebe-/Rotations-, Bit- und Kontroll-/Sprungbefehlen.
  - **Datentransfer:** ldm (load), stm (store), clr (clear)
  - **Arithmetik:** add, sub
  - **Kontrollbefehle:** halt

#### 4. Speicherorganisation
- **Programmspeicher (Flash):** Organisiert in 16-Bit-Worten mit Wortadressen.
- **Datenspeicher (SRAM):** Organisiert in 8-Bit-Worten mit Byteadressen.
- **Adressbereich:** Inklusive Register, I/O-Register und SRAM. Alle Komponenten am Bus haben eindeutige Adressen.

#### 5. Ein- und Ausgabeschnittstellen
- **Parallel-I/O-Ports:** Organisiert in Gruppen (Ports) entsprechend der Wortbreite des Prozessors (z.B. 8/16/32 Bits).
  - **Register der Parallel-Ports:** PORTx (Ausgangsregister), PINx (Eingangsregister), DDRx (Richtungsregister)

#### 6. Gehäuse und Anschlüsse
- **Gehäuseformen:** DIL (Dual in Line), TQFP (Thin Quad Flat Package), VQFN (Very Thin Quad Flat No Leads Package).
- **Spannungsversorgung:** Typisch 5V.
- **Konfiguration:** Mehrfachbelegung und konfigurierbare Bedeutung der Anschlüsse.

#### 7. Entwicklungsumgebung und Kosten
- **Entwicklungsumgebung:** Freie Entwicklungsumgebungen verfügbar, viele Tutorials und Unterlagen online.
- **Kosten:** Variieren je nach Ausbaustufe und Stückzahl (0,25 € bis 8 €).

### Ausführliche Zusammenfassung des Skripts "Architektur Atmel AVR-RISC"

#### 1. Einführung in die Architektur
- **RISC (Reduced Instruction Set Computer):** Der Atmel AVR-RISC Mikrocontroller ist ein moderner 8-Bit-Mikrocontroller mit einer Architektur, die speziell für eine reduzierte Anzahl an Maschinenbefehlen optimiert ist.
- **Harvard-Architektur:** Diese Architektur trennt den Programm- und Datenspeicher, was parallelen Zugriff auf Befehle und Daten ermöglicht und die Effizienz erhöht.

#### 2. Hauptkomponenten des ATmega32
- **Register:** Der ATmega32 verfügt über 32 allgemeine Arbeitsregister, die direkt mit der ALU (Arithmetic Logic Unit) verbunden sind, was schnelle Rechenoperationen ermöglicht.
- **Programmspeicher (Flash):** Der Programmspeicher reicht von 256 Byte bis 256 kByte, wobei der ATmega32 typischerweise 32 kByte hat.
- **Datenspeicher (SRAM):** Der Datenspeicher reicht von 0 bis 8 kByte, wobei der ATmega32 2 kByte hat.
- **Peripheriekomponenten:** Integrierte Komponenten wie parallele und serielle Schnittstellen (I2C, SPI), A/D-Wandler, Timer, CAN- und USB-Controller.

#### 3. Befehlsarchitektur
- **Befehlssatz des ATmega32:** Der Befehlssatz umfasst verschiedene Gruppen:
  - **Datentransferbefehle:** z.B. ldm (load direct from memory), stm (store direct to memory), clr (clear register)
  - **Arithmetische Befehle:** z.B. add (add without carry), sub (subtract without carry)
  - **Logische Befehle:** z.B. and (bitwise and), or (bitwise or)
  - **Schiebe-/Rotationsbefehle:** z.B. lsl (logical shift left), lsr (logical shift right)
  - **Bitbefehle:** z.B. cbr (clear bits in register), sbr (set bits in register)
  - **Kontroll-/Sprungbefehle:** z.B. jmp (jump), brne (branch if not equal)

#### 4. Speicherorganisation
- **Programmspeicher (Flash):** Organisiert in 16-Bit-Worten, die mittels Wortadressen adressiert werden.
- **Datenspeicher (SRAM):** Organisiert in 8-Bit-Bytes, die mittels Byteadressen adressiert werden.
- **Adressbereich:** Enthält Register, I/O-Register und SRAM. Alle Komponenten im Adressraum müssen eine eindeutige Adresse haben, was die Adressierung und Datenverwaltung vereinfacht.

#### 5. Ein- und Ausgabeschnittstellen
- **Parallel-I/O-Ports:** Organisation von Ein- und Ausgabeleitungen in Gruppen, sogenannte Ports, die der Wortbreite des Prozessors entsprechen (z.B. 8, 16 oder 32 Bit).
  - **Register der Parallel-Ports:** 
    - **PORTx:** Ausgangsregister für Daten
    - **PINx:** Eingangsregister zum Lesen von Daten
    - **DDRx:** Richtungsregister zur Festlegung der Richtung (Ein- oder Ausgang) der Portbits

#### 6. Gehäuse und Anschlüsse
- **Gehäuseformen:** Verfügbar in verschiedenen Formen wie DIL (Dual in Line), TQFP (Thin Quad Flat Package) und VQFN (Very Thin Quad Flat No Leads Package).
- **Spannungsversorgung:** Typischerweise 5V.
- **Anschlüsse:** Mehrfachbelegung und konfigurierbare Bedeutung der Anschlüsse ermöglichen flexible Einsatzmöglichkeiten und Anpassungen.

#### 7. Entwicklungsumgebung und Kosten
- **Entwicklungsumgebung:** Kostenlose Tools wie Microchip Studio und VS Code mit PlatformIO sind verfügbar, welche Debugging, Simulation und das Flashen des Mikrocontrollers unterstützen.
- **Kosten:** Abhängig von der Ausbaustufe und Stückzahl, variieren die Kosten von etwa 0,25 € bis 8 €.

#### 8. Beispielprogramm
Ein typisches Beispielprogramm zur Addition von vier 8-Bit-Zahlen:
```asm
ldi r16, 0
lds r17, Adresse(A)
add r16, r17
lds r17, Adresse(B)
add r16, r17
lds r17, Adresse(C)
add r16, r17
lds r17, Adresse(D)
add r16, r17
sts Adresse(E), r16
loop:
jmp loop
```
Dieses Programm illustriert die Verwendung von Befehlen zur Initialisierung, Datenübertragung und arithmetischen Operationen sowie eine einfache Schleife.

### Zusammenfassung der wichtigsten Konzepte:
- **Grundkomponenten:** Register, ALU, Steuerwerk, Speicher, Busse.
- **Befehlssatz:** Basisbefehle für Datenmanipulation und Steuerung.
- **Speicherorganisation:** Trennung von Programm- und Datenspeicher, eindeutige Adressierung.
- **I/O-Schnittstellen:** Konfiguration und Verwendung von Parallel-I/O-Ports.
- **Entwicklungsumgebung:** Verfügbarkeit und Kosten von Entwicklungswerkzeugen und Mikrocontrollern.


### Zusammenfassung des Skripts "Timer/Counter, AD/DA-Wandler"

#### 1. Timer/Counter - Zeitgeber/Zähler

**Aufgaben:**
- **Zählen von Ereignissen:** Zählt eingehende Impulse, die von externen Quellen wie Sensoren stammen können.
- **Messen von Zeiten:** Misst Zeitintervalle zwischen Ereignissen.
- **Erzeugung von Impulsfolgen:** Generiert regelmäßige Impulsfolgen zur Steuerung anderer Geräte.
- **Pulsweitenmodulation (PWM):** Steuert die Leistung von Motoren und LEDs durch Variation des Tastverhältnisses.
- **Periodische Interrupts:** Erzeugt regelmäßig Interrupts zur zeitlichen Steuerung von Prozessen.
- **Verzögerungen:** Implementiert Zeitverzögerungen in Programmen.

**Grundstruktur:**
- Besteht aus einem Zähler, Zählerstandsregister, Startwertregister, Kontrollregister, Vorteiler, und optionalen Vergleichs- und Captureregistern.

#### 2. Zählvorgänge und Timer-Perioden

**Aufwärtszähler:**
- Zählt von 0 bis 2^N-1 und erzeugt einen Überlaufinterrupt nach 2^N Takten.
- Die Timer-Periode \( T_P \) hängt von der Anzahl der Zählschritte \( 2^N \) und dem Eingangstakt \( T_T \) ab.

**Verwendung des Startwert-Registers:**
- Ermöglicht Variation der Timerperiode durch Setzen eines Startwerts, wodurch die effektive Zählperiode verkürzt wird.

**Erweiterte Grundstruktur:**
- Umfasst zusätzlich Vergleichs- und Captureregister, um komplexere Timerfunktionen wie PWM und Zeitmessung zu ermöglichen.

#### 3. Timer-Funktionen

**Impulszählung:**
- Erfasst externe Impulse und zählt sie zur Messung von Ereignishäufigkeiten.

**Messung von Impulsabständen:**
- Misst Zeitintervalle zwischen zwei erfassten Impulsen, wobei Überläufe berücksichtigt werden müssen.

**Frequenzgenerator:**
- Generiert Rechtecksignale mit variabler Frequenz und konstantem Tastverhältnis (CTC-Modus).

**PWM (Pulsweitenmodulation):**
- **Fast PWM:** Hohe Frequenz, typischerweise doppelt so hoch wie andere Methoden. Geeignet für DAC-Anwendungen und Lautsprechersteuerung.
- **Phase Correct PWM:** Konstante Periode, folgt einem Sinusverlauf. Geeignet für Spannungsregelung und sinusförmige Signalgeneratoren.

#### 4. ATmega32 Timer/Counter

**Struktur des 8-Bit-Timers (Timer0):**
- Inklusive Taktwahl, Steuerlogik, Zähler und Zählerstandsregister, Vergleichsregister.
- Unterstützt keine Capture-Möglichkeiten, diese bieten jedoch die 16-Bit-Timer.

**Watchdog-Timer (WDT):**
- Überwacht die CPU und führt einen Reset durch, wenn die CPU nicht innerhalb einer vorgegebenen Zeitspanne reagiert. Dies verhindert das "Hängenbleiben" des Programms.

#### 5. Analog-Digital-Umsetzer (ADC) und Digital-Analog-Umsetzer (DAC)

**DAC (Digital/Analog-Converter):**
- Wandelt digitale Werte in analoge Spannungen um, die durch PWM und nachgeschaltete Filter realisiert werden können.
- Typisch sind 8-, 10-, 12- oder 16-Bit-Auflösungen.

**ADC (Analog/Digital-Converter):**
- Wandelt analoge Spannungen in digitale Werte um, die von Sensoren stammen.
- Charakterisiert durch Eingangsspannungsbereich, Auflösung (typisch 8-, 10-, 12-Bit) und Umsetzzeit.
- Der ATmega32 verfügt über einen 10-Bit-ADC mit 8 Eingängen.

### Zusammenfassung der Themen "Timer/Counter, AD/DA-Wandler" und "Interruptverarbeitung" im Bezug auf die Klausuren

#### Timer/Counter und AD/DA-Wandler

**1. Aufgaben der Timer/Counter:**
- **Zählen von Ereignissen:** Erfassung von Impulsen, z.B. von Sensoren.
- **Messen von Zeiten:** Bestimmung von Zeitintervallen zwischen Ereignissen.
- **Erzeugung von Impulsfolgen:** Regelmäßige Signalgenerierung, z.B. für PWM.
- **Pulsweitenmodulation (PWM):** Steuerung der Leistungsabgabe an Geräte wie Motoren und LEDs.
- **Periodische Interrupts:** Regelmäßige Unterbrechungen zur Synchronisation oder Steuerung von Prozessen.
- **Verzögerungen:** Implementierung von Wartezeiten in Programmen.

**2. Grundlegende Struktur:**
- **Zähler:** Inkrementiert oder dekrementiert bei jedem Taktimpuls.
- **Zählerstandsregister:** Hält den aktuellen Stand des Zählers.
- **Startwertregister:** Initialwert des Zählers.
- **Kontrollregister:** Steuerung der Betriebsmodi.
- **Vorteiler:** Teilt den Eingangsclock, um die Zählfrequenz zu steuern.
- **Vergleichsregister:** Dient zur Generierung von PWM-Signalen und zur Bestimmung von Timer-Interrupts.
- **Capture-Register:** Speichert den Zählerstand bei bestimmten Ereignissen.

**3. Betriebsmodi:**
- **Aufwärtszähler:** Zählt von 0 bis zu einem Maximalwert (2^N-1).
- **Vergleichs-Reset:** Setzt den Zähler bei Erreichen eines Vergleichswerts zurück.
- **Capture-Modus:** Erfasst den Zählerstand bei einem bestimmten Signal.
- **PWM-Modi:** Erzeugt Signale mit variablem Tastverhältnis.

**4. ADC (Analog/Digital-Converter):**
- **Aufgabe:** Wandelt analoge Signale in digitale Werte um.
- **Auflösung:** Bestimmt die Genauigkeit der Umwandlung (typisch 8-, 10-, 12-Bit).
- **Umsetzprinzip:** Sukzessive Approximation, bei der der analoge Eingang mit einem generierten Referenzwert verglichen wird.

**5. DAC (Digital/Analog-Converter):**
- **Aufgabe:** Wandelt digitale Werte in analoge Signale um.
- **Typische Anwendungen:** Audioausgabe, Motorsteuerung.

#### Interruptverarbeitung

**1. Grundlagen:**
- **Definition:** Ein Interrupt ist ein Signal, das die normale Programmausführung unterbricht und die Ausführung einer speziellen Interrupt-Service-Routine (ISR) startet.
- **Arten von Interrupts:**
  - **Externe Interrupts:** Ausgelöst durch externe Ereignisse wie Taster oder Sensoren.
  - **Interne Interrupts:** Generiert durch Timer, ADCs oder andere interne Peripherien.
  - **Software-Interrupts:** Durch spezielle Befehle im Programmcode ausgelöst.

**2. Ablauf bei Interrupts:**
- **Unterbrechung der Hauptprogrammschleife:** Die aktuelle Ausführung wird gestoppt.
- **Sprung zur ISR:** Der Prozessor führt die ISR aus.
- **Rückkehr zur Hauptschleife:** Nach Beendigung der ISR wird die Hauptprogrammschleife fortgesetzt.

**3. Priorisierung und Maskierung:**
- **Priorisierung:** Bestimmt die Reihenfolge, in der gleichzeitige Interrupts behandelt werden.
- **Maskierung:** Ermöglicht das Ein- und Ausschalten von Interrupts zur Steuerung der Programmausführung.

**4. Verwendung in Mikrocontroller-Programmen:**
- **Timer-Interrupts:** Regelmäßige Zeitsteuerung von Prozessen.
- **ADC-Interrupts:** Verarbeitung von Sensorsignalen, sobald eine Umwandlung abgeschlossen ist.
- **Externe Interrupts:** Reaktion auf externe Ereignisse, z.B. Tastenbetätigungen.

### Bezug auf die Klausuren

In den Klausuren werden diese Themen durch verschiedene Arten von Fragen abgedeckt:
1. **Kurzfragen:** Grundlagen und Konzepte von Timer/Counter, ADC, DAC und Interruptverarbeitung.
2. **Berechnungsaufgaben:** Bestimmung von Timer-Perioden, Frequenzen, ADC-Auflösungen und Umsetzzeiten.
3. **Skizzieraufgaben:** Diagramme zur Darstellung von Zählerständen, PWM-Signalen und Ablauf von Interrupts.
4. **Programmierungsaufgaben:** Schreiben und Erklären von Programmen, die Timer, ADCs und Interrupts verwenden.

Typische Aufgaben könnten beinhalten:
- **Berechnung der Timerperiode:** \( T_P = (2^N - Startwert) \cdot T_{CPU} \cdot Vorteiler \)
- **Skizzierung eines PWM-Signals:** Darstellung der Pulse mit variabler Breite und deren Bedeutung.
- **Implementierung einer ISR:** Schreiben einer Interrupt-Service-Routine zur Verarbeitung von Timer-Interrupts.


### Zusammenfassung des Skripts "Interruptverarbeitung"

#### 1. Einführung in die Interruptverarbeitung

**Motivation:**
- **Polling:** Kontinuierliche Abfrage von Eingabewerten, verbraucht viel Rechenleistung.
- **Interrupts:** Effizienter, da der Prozessor nur bei Eintreten eines Ereignisses unterbrochen wird.

#### 2. Grundlagen der Interrupts

**Funktionsweise:**
- **Interrupts unterbrechen das laufende Programm:** Springen zu einer speziellen Routine (Interrupt-Service-Routine, ISR).
- **Nach der ISR:** Rückkehr zur unterbrochenen Programmausführung.
- **Prozessorkernstatus:** Wird auf dem Stack gespeichert und nach der ISR wiederhergestellt.

#### 3. Interrupts beim ATmega32

**Einsprungtabelle:**
- Interrupts besitzen eine Einsprungtabelle, die Adressen der ISRs enthält.
- **Beispielhafte Tabelle:**
  - URXCaddr (UART Empfangs-Complete)
  - UTXCaddr (UART Transmit-Complete)

**Beispielprogramm:**
```asm
rjmp Main
.ORG URXCaddr
jmp ISR_URXC
.ORG UTXCaddr
jmp ISR_UTXC
.ORG 0x2A
Main: ...
...
ISR_URXC:
...
reti
ISR_UTXC:
...
reti
```

#### 4. Interrupt-Strukturen

**Einfache Interruptstruktur:**
- **Implementierung:** Über ein ODER-Gatter zusammengeführte Interruptleitungen.
- **Interrupt-Flag (IF):** Zur Identifikation der Quelle.

**Ablauf der Interruptroutine:**
1. **Lesen der Interrupt-Flags (IF):** Bestimmung der Quelle.
2. **Verarbeitung:** Abarbeitung der entsprechenden ISR.
3. **Rückkehr:** Rückkehr zum Hauptprogramm mittels `RTI` (Return from Interrupt).

#### 5. Statische Interruptvektortabelle

**Eigenschaften:**
- **Priorisierung:** Statisch oder dynamisch durch die Interrupt-Logik der CPU.
- **Vorteile:** Klare Priorisierung und schnelle ISR-Ausführung.

#### 6. Dynamische Interruptvektortabelle

**Interruptcontroller:**
- **Register:**
  - **IMR (Interruptmask-Register):** Welche Interrupts maskiert sind.
  - **IRR (Interruptrequest-Register):** Anstehende Interruptanforderungen.
  - **ISR (Interruptstatus-Register):** Status der Interrupts.
- **Schaltnetz (PSN):** Priorisiert Interrupts.
- **IRQ (Interruptrequest) und IACK (Interruptacknowledge):** Signale für Anforderung und Bestätigung.

#### 7. Ablauf eines Interrupts

1. **Interruptanforderung:** Von einem Busteilnehmer an die CPU.
2. **CPU stoppt das aktuelle Programm:** Speichert den Status und springt zur ISR.
3. **Ausführung der ISR:** Bearbeitung des Interrupts.
4. **Rückkehr:** Wiederherstellung des Status und Fortsetzung des Hauptprogramms.

#### 8. Beispiele und Anwendungen

**Typische Anwendungen:**
- **Timer-Interrupts:** Regelmäßige Zeitsteuerung.
- **ADC-Interrupts:** Verarbeitung von Sensorsignalen.
- **Externe Interrupts:** Reaktion auf Tasten oder andere externe Ereignisse.

**Implementierung im ATmega32:**
```asm
.ORG ADCaddr
jmp ISR_ADC
.ORG Timeraddr
jmp ISR_Timer
...
ISR_ADC:
...
reti
ISR_Timer:
...
reti
```

#### Zusammenfassung der wichtigsten Konzepte:

- **Interruptarten:** Externe, interne und Software-Interrupts.
- **Prozessorkernstatus:** Speichern und Wiederherstellen.
- **Einsprungtabelle:** Statisch oder dynamisch.
- **Implementierung und Ablauf:** Vom Anfordern bis zur Abarbeitung und Rückkehr.

### Zusammenfassung des Skripts "Parallele und serielle Ein-/Ausgabe I"

#### 1. Parallele I/O

**Grundlagen:**
- Geräte können sowohl intern als auch extern über parallele Leitungen kommunizieren.
- **Synchronisationsmöglichkeiten:**
  - **Asynchron:** Steuerung erfolgt über zusätzliche Signalleitungen (RTS/CTS) oder spezielle Steuerzeichen (XON/XOFF).
  - **Synchron:** Übertragung des Taktsignals zusammen mit den Daten.

**Asynchrone Übertragung:**
- **Hardware-Steuerung:** 
  - **RTS (Request to Send):** Signalisiert die Bereitschaft des Senders.
  - **CTS (Clear to Send):** Signalisiert die Bereitschaft des Empfängers.
- **Software-Steuerung:** 
  - **XON/XOFF:** Steuerzeichen zur Steuerung des Datenflusses.

**Synchrone Übertragung:**
- **Taktübertragung:** Der Takt wird zusammen mit den Daten übertragen, um eine synchrone Kommunikation sicherzustellen.

#### 2. Serielle I/O

**Grundlagen der seriellen Kommunikation:**
- **UART (Universal Asynchronous Receiver/Transmitter):** Standard für serielle Kommunikation.
- **Funktionsprinzip:**
  - Daten werden bitweise über eine Leitung übertragen.
  - Sender und Empfänger sind durch Schieberegister verbunden.
  - Der Sender taktet die Bits in das Schieberegister, während der Empfänger diese Bits ausliest.

**Datenrahmen und Codierung:**
- **NRZ-Codierung (Non Return to Zero):** Standardmethode zur Codierung von seriellen Daten.
- **Datenrahmen:**
  - 1 Startbit
  - 7, 8 oder 9 Datenbits
  - Optional 1 Paritätsbit
  - 1 oder 2 Stopbits

**Fragen zur seriellen Kommunikation:**
1. Woher weiß der Empfänger, wann er das Schieberegister takten muss?
2. Woher weiß der Empfänger, dass eine Übertragung beginnt?
3. Woher weiß der Empfänger, wie schnell der Takt ist?
4. Woher weiß der Empfänger, wie viele Bits übertragen werden?
5. Warum wird der Takt nicht vom Sender zum Empfänger übertragen?
6. Was passiert bei Fehlern in der Übertragung?

**Beispiele für Datenformate:**
- **7 Datenbits, 1 Paritätsbit, 1 Stopbit:** Paritätsbit zur Fehlererkennung.
- **8 Datenbits, 1 oder 2 Stopbits:** Kein Paritätsbit, einfache Fehlererkennung durch Stopbits.

#### 3. UART-Schnittstelle des ATmega32

**Takterzeugung und Steuerung:**
- **Baudratenregister:** Zur Einstellung der Übertragungsgeschwindigkeit.
- **Baudratengenerator:** Erzeugt den erforderlichen Takt für die serielle Kommunikation.

**Sendeteil des UART-Ports:**
- **Senderegister:** Zum Schreiben von Daten, die gesendet werden sollen.
- **Schieberegister:** Taktet die Daten bitweise auf die Leitung.

**Empfangsteil des UART-Ports:**
- **Empfangsregister:** Zum Lesen der empfangenen Daten.
- **Schieberegister:** Empfängt die Daten bitweise von der Leitung.

**Steuer- und Statusregister:**
- **Steuerregister UCSRB:** Aktivieren von Sender und Empfänger, Steuerung der seriellen Kommunikation.

#### Klausurbezogene Inhalte:

In den Klausuren könnten die folgenden Themen abgefragt werden:
- **Unterschiede zwischen paralleler und serieller Kommunikation:** Vor- und Nachteile, Anwendungsbereiche.
- **Berechnung von Baudraten und Taktfrequenzen:** Einstellungen für UART-Übertragungen.
- **Aufbau und Ablauf von Datenübertragungen:** Zeichnen und erklären von Datenrahmen und Kommunikationsprotokollen.
- **Programmieraufgaben:** Implementierung von seriellen und parallelen Kommunikationsschnittstellen in Mikrocontroller-Programmen.

### Zusammenfassung des Skripts "Parallele und serielle Ein-/Ausgabe I"

#### 1. Parallele I/O

**Grundlagen:**
- Geräte können sowohl intern als auch extern über parallele Leitungen kommunizieren.
- **Synchronisationsmöglichkeiten:**
  - **Asynchron:** Steuerung erfolgt über zusätzliche Signalleitungen (RTS/CTS) oder spezielle Steuerzeichen (XON/XOFF).
  - **Synchron:** Übertragung des Taktsignals zusammen mit den Daten.

**Asynchrone Übertragung:**
- **Hardware-Steuerung:** 
  - **RTS (Request to Send):** Signalisiert die Bereitschaft des Senders.
  - **CTS (Clear to Send):** Signalisiert die Bereitschaft des Empfängers.
- **Software-Steuerung:** 
  - **XON/XOFF:** Steuerzeichen zur Steuerung des Datenflusses.

**Synchrone Übertragung:**
- **Taktübertragung:** Der Takt wird zusammen mit den Daten übertragen, um eine synchrone Kommunikation sicherzustellen.

#### 2. Serielle I/O

**Grundlagen der seriellen Kommunikation:**
- **UART (Universal Asynchronous Receiver/Transmitter):** Standard für serielle Kommunikation.
- **Funktionsprinzip:**
  - Daten werden bitweise über eine Leitung übertragen.
  - Sender und Empfänger sind durch Schieberegister verbunden.
  - Der Sender taktet die Bits in das Schieberegister, während der Empfänger diese Bits ausliest.

**Datenrahmen und Codierung:**
- **NRZ-Codierung (Non Return to Zero):** Standardmethode zur Codierung von seriellen Daten.
- **Datenrahmen:**
  - 1 Startbit
  - 7, 8 oder 9 Datenbits
  - Optional 1 Paritätsbit
  - 1 oder 2 Stopbits

**Fragen zur seriellen Kommunikation:**
1. Woher weiß der Empfänger, wann er das Schieberegister takten muss?
2. Woher weiß der Empfänger, dass eine Übertragung beginnt?
3. Woher weiß der Empfänger, wie schnell der Takt ist?
4. Woher weiß der Empfänger, wie viele Bits übertragen werden?
5. Warum wird der Takt nicht vom Sender zum Empfänger übertragen?
6. Was passiert bei Fehlern in der Übertragung?

**Beispiele für Datenformate:**
- **7 Datenbits, 1 Paritätsbit, 1 Stopbit:** Paritätsbit zur Fehlererkennung.
- **8 Datenbits, 1 oder 2 Stopbits:** Kein Paritätsbit, einfache Fehlererkennung durch Stopbits.

#### 3. UART-Schnittstelle des ATmega32

**Takterzeugung und Steuerung:**
- **Baudratenregister:** Zur Einstellung der Übertragungsgeschwindigkeit.
- **Baudratengenerator:** Erzeugt den erforderlichen Takt für die serielle Kommunikation.

**Sendeteil des UART-Ports:**
- **Senderegister:** Zum Schreiben von Daten, die gesendet werden sollen.
- **Schieberegister:** Taktet die Daten bitweise auf die Leitung.

**Empfangsteil des UART-Ports:**
- **Empfangsregister:** Zum Lesen der empfangenen Daten.
- **Schieberegister:** Empfängt die Daten bitweise von der Leitung.

**Steuer- und Statusregister:**
- **Steuerregister UCSRB:** Aktivieren von Sender und Empfänger, Steuerung der seriellen Kommunikation.

#### Klausurbezogene Inhalte:

In den Klausuren könnten die folgenden Themen abgefragt werden:
- **Unterschiede zwischen paralleler und serieller Kommunikation:** Vor- und Nachteile, Anwendungsbereiche.
- **Berechnung von Baudraten und Taktfrequenzen:** Einstellungen für UART-Übertragungen.
- **Aufbau und Ablauf von Datenübertragungen:** Zeichnen und erklären von Datenrahmen und Kommunikationsprotokollen.
- **Programmieraufgaben:** Implementierung von seriellen und parallelen Kommunikationsschnittstellen in Mikrocontroller-Programmen.

### Zusammenfassung des Skripts "Systembus I"

#### 1. Wiederholung - Architektur Atmel AVR-RISC
- **Einbindung in den Adressraum:** Register, I/O-Register und SRAM werden in den Adressbereich des Prozessors eingeblendet.
- **Maschinenbefehle:** Basisbefehle für Datentransfer, Arithmetik, Logik, Schiebe-/Rotationsoperationen, Bitoperationen und Kontroll-/Sprungbefehle.
- **Statusregister:** Enthält Flags wie H (Half Carry), C (Carry), Z (Zero), S (Sign), V (Overflow), N (Negative).

#### 2. Einführung in den Systembus

**Architektur des Modellrechners:**
- **Komponenten:** Hauptspeicher, A/D- und D/A-Wandler, CAN, Steuerwerk, Rechenwerk (ALU), Register, I/O-Geräte.
- **Systembus:** Verbindet Speicher und Ein-/Ausgabeeinheiten im Mikrorechner. Besteht aus Datenbus, Adressbus und Steuerbus.

**Von-Neumann-Architektur:**
- Einheitlicher Speicher für Programme und Daten.
- **Komponenten:**
  - **CPU:** Bestehend aus Steuerwerk, Rechenwerk (ALU), Register.
  - **Bus:** Verbindet die CPU mit Speicher und I/O-Geräten.
  - **Adresswerk:** Generiert Speicheradressen.

#### 3. Busprinzip

**Grundprinzipien:**
- Nur ein Teilnehmer darf gleichzeitig senden.
- Teilnehmer müssen eindeutig adressierbar sein.
- Kommunikation erfolgt nach einem festgelegten Busprotokoll.
- Busse können sowohl parallel als auch seriell Informationen übertragen.

**Busstrukturen:**
- **Datenbus:** Überträgt Daten zwischen Komponenten.
- **Adressbus:** Überträgt Adressen der Speicherzellen oder I/O-Geräte.
- **Steuerbus:** Überträgt Steuerinformationen und Kontrollsignale.

#### 4. Adressierung im Systembus

**Adresszuweisung:**
- Jeder Teilnehmer am Bus (Speicher, I/O-Schnittstellen, Register) benötigt eine eindeutige Adresse.
- **Beispiel für ATmega32:**
  - Einblendung von Registern, I/O-Registern und SRAM in den Adressbereich.
  - Verwendung von Chip-Select (CS) für die Auswahl der Speicherzellen.

**Adressdekodierung:**
- **Aufgabe der CS-Logik:** Selektion der richtigen Speicherzelle oder I/O-Bausteins anhand der Adressleitungen.
- **Beispiel:** Systembus mit 16 Adressleitungen und 8 Datenleitungen, Speicherbaustein mit 10 Adressleitungen, E/A-Baustein mit 2 Adressleitungen.

#### 5. Physikalische Busanschaltung

**Probleme und Lösungen:**
- **Problem:** Nur ein Ausgang darf den Pegel auf der Busleitung bestimmen.
- **Gegentaktausgangsstufe:** Zwei Transistoren, die den Buspegel bestimmen.
- **Open-Collector-Ausgangsstufe:** Verwendet für Steuerleitungen, aktiver Pegel ist LOW.
- **Tri-State-Ausgangsstufe:** Daten- und Adressleitungen, drei Zustände (HIGH, LOW, hochohmig).

**Verwendung:**
- **Gegentaktausgangsstufe:** Normale 1-zu-1-Gatterverbindungen.
- **Open-Collector-Ausgangsstufe:** Steuerleitungen.
- **Tri-State-Ausgangsstufe:** Daten- und Adressleitungen, Freischaltung durch Steuerleitungen.

#### 6. Fragen zum Systembus

- **Wie erfolgt die Adressierung?**
- **Wie erfolgt die physikalische Busanschaltung?**
- **Welche Steuerleitungen gibt es?**
- **Wie erfolgt die Buszuteilung?**
- **Wie erfolgt die zeitliche Steuerung?**

### Zusammenfassung des Skripts "Systembus II"

#### 1. Fragen zum Systembus
- **Adressierung:** Wie werden die Adressen den verschiedenen Komponenten zugewiesen?
- **Physikalische Busanschaltung:** Wie werden die Geräte physikalisch an den Bus angeschlossen?
- **Steuerleitungen:** Welche Steuerleitungen gibt es und welche Aufgaben haben sie?
- **Buszuteilung:** Wie wird der Bus zwischen verschiedenen Geräten geteilt?
- **Zeitliche Steuerung:** Wie wird die Zeitsteuerung im Bus realisiert?

#### 2. Zeitverhalten im Systembus

**Synchroner Bus:**
- **Zeitverhalten:** Alle Ereignisse erfolgen zu festen, vorgegebenen Zeitpunkten im Raster des Systemtaktes.
- **Eigenschaften:**
  - Starres Zeitverhalten benötigt wenig Steuerleitungen und geringen Steueraufwand, nutzt aber Ressourcen suboptimal.

**Semi-Synchroner Bus:**
- **Prinzip:** Prinzipiell synchrones Verhalten des Busses, jedoch signalisiert der Speicher durch ein READY-Signal, ob die synchrone Übertragung in einem Takt möglich ist.
- **Eigenschaften:**
  - Ermöglicht die Verwendung von langsameren Peripheriegeräten durch Einfügen von Wartezyklen.
  - Bsp.: Intel x86

**Asynchroner Bus:**
- **Prinzip:** Kein festes Zeitraster (kein Taktsignal).
- **Eigenschaften:**
  - Kommunikation kann zu jedem Zeitpunkt initiiert werden.
  - Steuerung der Schritte über Handshake-Verfahren.
  - Anpassung an die Geschwindigkeit beider Partner.
  - I.d.R. langsamer als semi-synchrone Lösungen, da die CPU an sich ein synchrones Schaltwerk ist.

#### 3. Ablauf eines Buszugriffs

**Lesen eines Speicherinhalts:**
1. CPU gibt die Adresse auf den Adressbus.
2. CPU setzt das R/#W-Signal auf dem Steuerbus.
3. Speicher dekodiert die Adresse und die Transferrichtung.
4. Speicher stellt das Datum auf dem Datenbus bereit.
5. CPU liest das Datum vom Datenbus.
6. CPU nimmt die Adresse und das R/#W-Signal zurück.

**Schreiben in eine Speicherzelle:**
1. CPU gibt die Adresse auf den Adressbus.
2. CPU setzt das #R/W-Signal auf dem Steuerbus.
3. Speicher dekodiert die Adresse und die Transferrichtung.
4. CPU stellt das Datum auf dem Datenbus bereit.
5. Speicher übernimmt das Datum vom Datenbus.
6. CPU nimmt die Adresse und das #R/W-Signal zurück.

#### 4. Fortgeschrittene Varianten des Systembusses

**Taktvervielfachung:**
- Nutzung beider Taktflanken für den Datentransfer (Double Data Rate RAM).

**Überlappende Adressierung:**
- Während eines Datentransfers wird bereits die nächste Adresse ausgegeben.

**Blockübertragung:**
- Transfer eines Blocks aufeinanderfolgender Bytes wird mit einer Adressierung angestoßen.

**Adress-/Daten-Multiplex:**
- Busleitungen werden erst zur Übertragung der Adresse genutzt, anschließend werden auf denselben Leitungen die Daten übertragen (z.B. PCI-Bus).

#### 5. Zeitspezifikation im synchronen Systembus

**Wichtige Zeitparameter:**
- **TAD:** Adressausgabeverzögerung
- **TRL:** R/#W-Verzögerung vor fallender Flanke
- **TRH:** R/#W-Verzögerung vor steigender Flanke
- **TDS:** Dateneinrichtezeit
- **TDH:** Datenhaltezeit

**Beispiel:**
- **BusTakt:** 100 MHz
- **Buszyklus:** 10 ns
- **Speicherzugriffszeit:** 15 ns
- **Signalübergangszeit:** 1 ns

#### 6. Kenndaten des Systembusses

**Datenbus:**
- 8/16/32/64 Datenleitungen, bidirektional
- Befehle laden, Operanden laden und speichern

**Adressbus:**
- Anzahl der Leitungen abhängig vom Adressraum des Prozessors
- Auswahl von Speicherstellen und I/O-Schnittstellen

**Steuerbus:**
- Unidirektional oder bidirektional
- Umschaltung der Datenrichtung (R/W)
- Gültigkeit der Signale anzeigen
- Zugriffskontrolle auf den Bus
- Unterbrechungsbehandlung
- Takt und Reset

**Hinweis:**
- Jede Prozessorfamilie hat eigene Signale, die ähnlich, aber nicht identisch sind.
- Früher: Systembus = Peripheriebus
- Heute: Einsatz dedizierter, genormter Peripheriebusse (z.B. PCI)

### Zusammenfassung des Skripts "Speicher/Speicherhierarchien"

#### 1. Einführung in Speichertechnologie

**Überblick:**
- **Speicherarten:**
  - **Elektrisch mit Rückkopplung:** Flip-Flop
  - **Elektrisch mit gespeicherter Ladung:** Dynamischer Halbleiterspeicher (Kondensatoren)
  - **Magnetisch:** Kernspeicher, Magnetband, Plattenspeicher
  - **Strukturell:** Lochkarten, Lochstreifen
  - **Räumlich:** Magnetblasenspeicher
  - **Optisch:** CD, DVD, Blu-Ray

**Definition:**
- **Speichern:** Veränderung physikalischer Eigenschaften zur Informationsspeicherung und deren Auslesbarkeit.

#### 2. Halbleiterspeicher

**ROM (Read Only Memory):**
- **Arten:** PROM, EPROM, EEPROM, FLASH
- **Eigenschaften:** Irreversibel (ROM, PROM) oder reversibel (EPROM, EEPROM, FLASH)

**RAM (Random Access Memory):**
- **Arten:** SRAM (statisch), DRAM (dynamisch)
- **Eigenschaften:** Wahlfreier Zugriff (unabhängig von der Speicherzellenlage)

**Terminologie:**
- **Speicherzelle:** Kleinste Informationseinheit (1 Bit)
- **Speicherwort:** Maximale Anzahl von Zellen, die in einem Buszyklus übertragen werden können
- **Physikalische Adresse:** Ortsangabe für eine Speicherzelle

#### 3. Halbleiterspeicher im Detail

**SRAM (Static RAM):**
- **Grundprinzip:** Flip-Flop-Schaltung, schneller Zugriff, hoher Platzbedarf (6 Transistoren pro Zelle)
- **Nutzung:** Cache-Speicher

**NVRAM (Non-Volatile RAM):**
- **Eigenschaften:** Nicht-flüchtiger Speicher, Kombination aus SRAM und EEPROM

**DRAM (Dynamic RAM):**
- **Funktionsprinzip:** Speichern von Informationen durch Kondensatoren, regelmäßiges Auffrischen notwendig
- **Nutzung:** Höchstintegrierte Speicher

**EPROM (Erasable Programmable ROM):**
- **Programmierung:** UV-Licht zum Löschen
- **Nutzung:** Mehrere hundert Löschzyklen möglich

**EEPROM (Electrically Erasable Programmable ROM):**
- **Programmierung:** Elektrisch löschbar und beschreibbar
- **Nutzung:** Feldprogrammierbare Systeme

**FLASH-Speicher:**
- **Eigenschaften:** Ähnlich wie EEPROM, aber schnellere Löschvorgänge durch Blocklöschung

#### 4. Speicherhierarchien

**Problem:**
- CPU-Leistung und Speicherleistung driften auseinander, was die CPU ausbremst.

**Lösungsansatz:**
- **Speicherhierarchien:** Kombination schneller und teurer Speicherarten (nah am Prozessor) mit langsameren und größeren Speichern (fern vom Prozessor)
- **Lokalitätsprinzip:**
  - **Zeitliche Lokalität:** Daten, die einmal benutzt wurden, werden oft kurz danach wieder genutzt.
  - **Räumliche Lokalität:** Programme nutzen oft nacheinander Daten, die räumlich benachbart im Speicher stehen.

**Beispiel einer Hierarchie:**
- **Cache-Speicher:** Schnell, klein, teuer (SRAM)
- **Hauptspeicher:** Langsamer, größer, günstiger (DRAM)
- **Sekundärspeicher:** Sehr langsam, sehr groß, sehr günstig (Festplatten, optische Speicher)

#### 5. Cache-Organisation

**Eigenschaften:**
- **Cache:** Schneller, kleiner Speicher zwischen CPU und Hauptspeicher, der meistgenutzte Daten hält
- **Grundprinzipien:**
  - **Look-Through-Cache:** Cache liegt zwischen CPU und Hauptspeicher
  - **Look-Aside-Cache:** Cache liegt parallel zum Hauptspeicher

**Cache-Design:**
- **Fragen:**
  - Welche Daten werden im Cache gehalten?
  - Wie findet man die Daten im Cache?
  - Wie werden Daten im Cache ersetzt?

**Speicherhierarchien und Effizienzsteigerung:**
- **Hierarchische Anordnung:** Mehrere Ebenen von Caches, wobei jede tiefere Ebene größer, aber langsamer ist.
- **Beispiel:** Pentium III mit separatem Befehlscache und Datencache

#### 6. Virtueller Speicher

**Grundprinzip:**
- **Virtueller Speicher:** Erweiterung des physikalischen Speichers durch Nutzung von Sekundärspeicher
- **Vorteil:** Programmierer muss keine Rücksicht auf physikalische Speichergröße nehmen
- **Nachteil:** Seitenaus- und -einlagerung kostet Zeit

**Adressübersetzung:**
- **Memory Management Unit (MMU):** Übersetzt virtuelle Adressen in physikalische Adressen
- **Translation Lookaside Buffer (TLB):** Beschleunigt Adressübersetzung durch Zwischenspeichern der häufig genutzten Adressübersetzungen

### Zusammenfassung des Skripts "RISC-CISC und Superskalarität"

#### 1. Einführung

**RISC (Reduced Instruction Set Computer):**
- **Motivation:** Untersuchung der Häufigkeit von Maschinenbefehlen in Programmen (IBM, Hennessy, Patterson).
- **Philosophie:** Weniger und einfachere Anweisungen.
- **Eigenschaften:**
  - 10 Befehle decken 80% des Codes ab, 21 Befehle 95%, 30 Befehle 99%.
  - Weniger komplexe Befehle ermöglichen schnellere Hardware und geringeren Platzbedarf.
  - Optimierung durch Compiler.
  - Register-orientierter Befehlssatz mit vielen Registern (typisch 32).

**CISC (Complex Instruction Set Computer):**
- **Motivation:** Geschwindigkeitsunterschied zwischen Prozessor und Hauptspeicher.
- **Philosophie:** Viele und komplexe Anweisungen, um Programmieraufgaben direkt in Maschinenbefehlen umzusetzen.
- **Eigenschaften:**
  - Kompakte Codierung spart Speicherplatz.
  - Befehlssätze oft stack-orientiert mit expliziten Push und Pop Anweisungen.
  - Wenige Register, komplexe Adressierungsmodi.
  - Beispiel: Intel x86/AMD x86.

#### 2. Steuerwerk und Befehlssätze

**CISC-Steuerwerk:**
- Aufwendiges Steuerwerk, oft mit Mikroprogramm.
- Befehle haben unterschiedliche Längen (1-16 Byte).
- Typische Vertreter: Intel x86, Motorola 68x00.

**RISC-Steuerwerk:**
- Einfacheres Steuerwerk, meist festverdrahtet.
- Einheitliches Befehlsformat.
- Typische Vertreter: MIPS, ARM, PowerPC.

#### 3. Leistungsbeurteilung

**Metriken:**
- **Befehlsdurchsatz:** Anzahl der ausgeführten Befehle pro Sekunde.
- **Ausführungszeit:** Zeit zur Ausführung eines Programms, abhängig von Betriebssystem, Speicher, Cache, Busorganisation, Taktrate und Architektur.

**Benchmarking:**
- Referenztestprogramme auf Referenzmaschine.
- Beispiel: SPEC-Benchmark.
- Gesamtbenchmark als geometrisches Mittel der Einzelzeiten.

**Ausführungszeit eines Programms:**
- Formel: \( T_{Execute} = N_{Instructions} \times CPI \times (Zeit / Takt) \)
- **NInstructions:** Anzahl der Befehle.
- **CPI:** Zyklen pro Instruktion.

#### 4. Pipelining

**Grundprinzip:**
- Ein Befehl wird in einer k-stufigen Pipeline in k Takten ausgeführt.
- In jedem Takt wird ein neuer Befehl geladen.
- Idealerweise werden k Befehle gleichzeitig behandelt.

**Konflikte:**
- **Datenkonflikte:** Echte Datenabhängigkeiten zwischen Befehlen (RAW-Hazard).
- **Steuerflusskonflikte:** Unklare Befehlsfolge bei Sprüngen.
- **Strukturkonflikte:** Mehrere Pipeline-Stufen benötigen dieselbe Ressource.

**Lösungen:**
- **Software:** NOP-Befehle einfügen, Befehle umordnen.
- **Hardware:** Forwarding, Sprungvorhersage, Pipeline-Stalling.

#### 5. Superskalarität

**Konzept:**
- Nutzung mehrerer paralleler Pipelines für verschiedene Befehlstypen (z.B. Integer, Floating Point).
- Befehls-Scheduler verteilt Befehle an parallele Ressourcen.
- Ermöglicht die gleichzeitige Ausführung mehrerer Befehle.

**Beispielarchitektur:**
- Mehrere Dekoder, Ganzzahl- und Gleitkomma-Rechenwerke, Lade-/Speichereinheiten.
- Registerumbenennung zur Vermeidung unnötiger Datenabhängigkeiten.

**Vorteile und Herausforderungen:**
- Beschleunigung der Programmausführung durch parallele Befehlsabarbeitung.
- Hoher Verwaltungsaufwand für den Prozessor.
- Problematisch bei Registerabhängigkeiten und Interrupts.

#### 6. Hyperthreading

**Prinzip:**
- Simultanes Multithreading mit einer CPU.
- Wichtige Ressourcen (Register, Rechenwerke) werden verdoppelt.
- Zwei Threads können nahezu parallel ablaufen.
- Rechenleistung: 1 CPU < Hyperthreading < 2 CPUs.


